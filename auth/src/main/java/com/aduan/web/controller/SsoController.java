package com.aduan.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sso")
public class SsoController {

    @RequestMapping("/test")
    public String test() {
        return "sso test";
    }
}
